package com.co.lbc.app.application.infrastructure.dummy;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import com.co.lbc.app.aplication.domain.Employee;
import com.co.lbc.app.aplication.domain.EmployeeId;
import com.co.lbc.app.application.infrastructure.dummy.DummyApiEmployeeRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@AutoConfigureMockMvc
@Disabled //This is not deterministic.
class DummyApiEmployeeRepositoryTestIT {

    @Autowired
    DummyApiEmployeeRepository repository;

    @Test
    void get_all_employees() {
        List<Employee> employees = repository.findAll();

        assertTrue(employees.size() > 0);
    }

    @Test
    void get_one_employee() {
        Optional<Employee> optionalEmployee = repository.findById(new EmployeeId(1L));
        assertTrue(optionalEmployee.isPresent());
    }

}