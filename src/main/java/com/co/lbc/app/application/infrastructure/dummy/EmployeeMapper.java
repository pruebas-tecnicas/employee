package com.co.lbc.app.application.infrastructure.dummy;

import java.math.BigDecimal;

import com.co.lbc.app.aplication.domain.*;

public class EmployeeMapper {
    public static Employee fromEntityApi(EmployeeApiEntity employeeApiEntity) {

        Long salary = employeeApiEntity.getSalary();
        EmployeeSalary employeeSalary = salary != null ? new EmployeeSalary(new BigDecimal(salary)) : null;

        Integer age = employeeApiEntity.getAge();
        EmployeeAge employeeAge = age != null ? new EmployeeAge(age) : null;

        return new Employee(
                new EmployeeId(employeeApiEntity.getId()),
                new EmployeeName(employeeApiEntity.getEmployeeName()),
                employeeSalary,
                employeeAge,
                new EmployeeImage(employeeApiEntity.getImage())
        );
    }
}
