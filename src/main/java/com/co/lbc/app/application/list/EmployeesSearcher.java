package com.co.lbc.app.application.list;

import java.util.List;

import com.co.lbc.app.aplication.domain.Employee;
import com.co.lbc.app.aplication.domain.EmployeeRepository;

public class EmployeesSearcher {
    private final EmployeeRepository repository;

    public EmployeesSearcher(EmployeeRepository repository) {
        this.repository = repository;
    }

    public List<Employee> run() {
        return repository.findAll();
    }
}
