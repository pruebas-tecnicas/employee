package com.co.lbc.app.application.infrastructure.persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.co.lbc.app.aplication.domain.Employee;
import com.co.lbc.app.aplication.domain.EmployeeId;
import com.co.lbc.app.aplication.domain.EmployeeRepository;

import java.util.List;
import java.util.Optional;

@Repository
@Qualifier("ProxyEmployeeRepository")
@Primary
public class ProxyEmployeeRepository implements EmployeeRepository {

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    @Qualifier("DummyApiEmployeeRepository")
    EmployeeRepository dummyApiEmployeeRepository;

    @Autowired
    @Qualifier("H2EmployeeRepository")
    EmployeeRepository inMemoryEmployeeRepository;


    @Override
    public Optional<Employee> findById(EmployeeId id) {
        try {
            Optional<Employee> optionalEmployee = dummyApiEmployeeRepository.findById(id);

            optionalEmployee.ifPresent(employee -> ((H2EmployeeRepository) inMemoryEmployeeRepository).refresh(employee));

            return optionalEmployee;

        } catch (Exception e) {
            LOGGER.info("getting employee from local database");
            return inMemoryEmployeeRepository.findById(id);
        }
    }

    @Override
    public List<Employee> findAll() {
        try {
            List<Employee> employees = dummyApiEmployeeRepository.findAll();

            ((H2EmployeeRepository) inMemoryEmployeeRepository).refresh(employees);
            return employees;

        } catch (Exception e) {
            LOGGER.info("getting employees from local database");
            return inMemoryEmployeeRepository.findAll();
        }
    }
}
