package com.co.lbc.app.application.shared.infrastructure.http;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.co.lbc.app.aplication.domain.EmployeeNotFound;
import com.co.lbc.app.application.shared.domain.DomainError;

@RestControllerAdvice
public class RestErrorHandler {
    @ExceptionHandler(value = {EmployeeNotFound.class})
    public ResponseEntity<ErrorResponse> handleIllegalEmployeeNotFound(EmployeeNotFound ex, WebRequest request) {
        return sendErrorResponse(ex.getLocalizedMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {DomainError.class})
    public ResponseEntity<ErrorResponse> handleDomainError(DomainError ex, WebRequest request) {
        ex.getStackTrace();
        return sendErrorResponse(ex.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    protected ResponseEntity<ErrorResponse> sendErrorResponse(String error, HttpStatus status) {

        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<>(new ErrorResponse(error), httpHeaders, status);
    }
}
