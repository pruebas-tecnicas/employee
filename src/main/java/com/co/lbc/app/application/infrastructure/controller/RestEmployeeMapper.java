package com.co.lbc.app.application.infrastructure.controller;

import com.co.lbc.app.aplication.domain.Employee;

public class RestEmployeeMapper {
    public static RestEmployee fromAggregate(Employee employee) {
        return new RestEmployee(
                employee.getId().value(),
                employee.getName().value(),
                employee.getSalary().value().longValue(),
                employee.anualSalary().longValue(),
                employee.getImage().value(),
                employee.getAge().value()
        );
    }
}
