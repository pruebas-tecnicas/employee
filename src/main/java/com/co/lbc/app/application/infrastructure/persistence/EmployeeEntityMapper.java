package com.co.lbc.app.application.infrastructure.persistence;

import java.math.BigDecimal;

import com.co.lbc.app.aplication.domain.*;

public class EmployeeEntityMapper {
    public static Employee fromEntity(EmployeeEntity employeeEntity) {

        Long salary = employeeEntity.getSalary();
        EmployeeSalary employeeSalary = salary != null ? new EmployeeSalary(new BigDecimal(salary)) : null;

        Integer age = employeeEntity.getAge();
        EmployeeAge employeeAge = age != null ? new EmployeeAge(age) : null;

        return new Employee(
                new EmployeeId(employeeEntity.getId()),
                new EmployeeName(employeeEntity.getName()),
                employeeSalary,
                employeeAge,
                new EmployeeImage(employeeEntity.getImage())
        );
    }

    public static EmployeeEntity fromAggregate(Employee employee) {
        return new EmployeeEntity(
                employee.getId().value(),
                employee.getName().value(),
                employee.getSalary().value().longValue(),
                employee.getAge().value(),
                employee.getImage().value()
        );
    }
}
