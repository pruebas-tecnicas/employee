package com.co.lbc.app.application.shared.domain;

public class DomainError extends RuntimeException {
    protected DomainError(String message) {
        super(message);
    }
}
