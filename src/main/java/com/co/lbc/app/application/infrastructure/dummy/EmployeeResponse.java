package com.co.lbc.app.application.infrastructure.dummy;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeResponse {
    private String status;

    @JsonProperty("data")
    private EmployeeApiEntity employee;

    @JsonProperty("message")
    private String message;
}
