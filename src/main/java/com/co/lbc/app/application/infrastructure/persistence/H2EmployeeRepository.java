package com.co.lbc.app.application.infrastructure.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.co.lbc.app.aplication.domain.Employee;
import com.co.lbc.app.aplication.domain.EmployeeId;
import com.co.lbc.app.aplication.domain.EmployeeRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
@Qualifier("H2EmployeeRepository")
public class H2EmployeeRepository implements EmployeeRepository {

    @Autowired
    JpaEmployeeRepository jpaEmployeeRepository;

    @Override
    public Optional<Employee> findById(EmployeeId id) {
        return jpaEmployeeRepository.findById(id.value()).map(EmployeeEntityMapper::fromEntity);
    }

    @Override
    public List<Employee> findAll() {
        return jpaEmployeeRepository.findAll().stream().map(EmployeeEntityMapper::fromEntity)
                .collect(Collectors.toList());
    }

    public void refresh(List<Employee> employees) {

        List<EmployeeEntity> employeeEntities = employees.stream().map(EmployeeEntityMapper::fromAggregate)
                .collect(Collectors.toList());

        jpaEmployeeRepository.saveAll(employeeEntities);
    }

    public void refresh(Employee employee) {
        jpaEmployeeRepository.save(EmployeeEntityMapper.fromAggregate(employee));
    }
}
