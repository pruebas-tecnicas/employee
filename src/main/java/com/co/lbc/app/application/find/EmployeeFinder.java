package com.co.lbc.app.application.find;

import com.co.lbc.app.aplication.domain.Employee;
import com.co.lbc.app.aplication.domain.EmployeeId;
import com.co.lbc.app.aplication.domain.EmployeeNotFound;
import com.co.lbc.app.aplication.domain.EmployeeRepository;


public class EmployeeFinder {

    private final EmployeeRepository repository;

    public EmployeeFinder(EmployeeRepository repository) {
        this.repository = repository;
    }

    public Employee run(Long id) {
        return repository.findById(new EmployeeId(id)).orElseThrow(() -> new EmployeeNotFound("Employee not found"));
    }
}
