package com.co.lbc.app.application.infrastructure.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RestEmployee {
    private Long id;

    private String name;

    private Long salary;

    @JsonProperty("anual_salary")
    private Long anualSalary;

    private String image;

    private Integer age;
}
