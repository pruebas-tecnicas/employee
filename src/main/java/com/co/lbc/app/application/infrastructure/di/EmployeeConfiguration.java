package com.co.lbc.app.application.infrastructure.di;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.co.lbc.app.aplication.domain.EmployeeRepository;
import com.co.lbc.app.application.find.EmployeeFinder;
import com.co.lbc.app.application.list.EmployeesSearcher;

@Configuration
public class EmployeeConfiguration {

    private final EmployeeRepository repository;

    public EmployeeConfiguration(EmployeeRepository repository) {
        this.repository = repository;
    }

    @Bean
    public EmployeeFinder employeeFinder() {
        return new EmployeeFinder(repository);
    }

    @Bean
    public EmployeesSearcher employeesSearcher() {
        return new EmployeesSearcher(repository);
    }
}
