package com.co.lbc.app.application.infrastructure.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaEmployeeRepository extends JpaRepository<EmployeeEntity, Long> {
}
