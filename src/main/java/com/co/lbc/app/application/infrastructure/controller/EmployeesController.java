package com.co.lbc.app.application.infrastructure.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import com.co.lbc.app.application.find.EmployeeFinder;
import com.co.lbc.app.application.list.EmployeesSearcher;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET})
public class EmployeesController {

    private final EmployeeFinder employeeFinder;
    private final EmployeesSearcher employeesSearcher;

    public EmployeesController(EmployeeFinder employeeFinder, EmployeesSearcher employeesSearcher) {
        this.employeeFinder = employeeFinder;
        this.employeesSearcher = employeesSearcher;
    }

    @GetMapping(value = "/employees", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public List<RestEmployee> list() {
        return employeesSearcher.run().stream()
                .map(RestEmployeeMapper::fromAggregate)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/employees/{id}", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public RestEmployee find(@PathVariable Long id) {
        return RestEmployeeMapper.fromAggregate(employeeFinder.run(id));
    }
}
