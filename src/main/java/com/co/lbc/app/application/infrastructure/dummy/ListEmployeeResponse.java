package com.co.lbc.app.application.infrastructure.dummy;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListEmployeeResponse {
    private String status;

    @JsonProperty("data")
    private List<EmployeeApiEntity> employees;

    @JsonProperty("message")
    private String message;
}
