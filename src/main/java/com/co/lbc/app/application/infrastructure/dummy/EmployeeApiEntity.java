package com.co.lbc.app.application.infrastructure.dummy;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeApiEntity {

    private Long id;

    @JsonProperty("employee_name")
    private String employeeName;

    @JsonProperty("employee_salary")
    private Long salary;

    @JsonProperty("employee_age")
    private Integer age;

    @JsonProperty("profile_image")
    private String image;

}
