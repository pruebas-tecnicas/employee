package com.co.lbc.app.application.infrastructure.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.co.lbc.app.aplication.domain.Employee;
import com.co.lbc.app.aplication.domain.EmployeeId;
import com.co.lbc.app.aplication.domain.EmployeeRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
@Qualifier("DummyApiEmployeeRepository")
public class DummyApiEmployeeRepository implements EmployeeRepository {

    Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Value("${api.employees_url}")
    private String EMPLOYEES_URL;

    @Value("${api.employee_url}")
    private String EMPLOYEE_URL;

    @Autowired
    RestTemplate restTemplate;

    @Override
    public Optional<Employee> findById(EmployeeId id) {
        try {
            ResponseEntity<EmployeeResponse> response = restTemplate
                    .getForEntity(EMPLOYEE_URL + "/" + id.value(), EmployeeResponse.class);

            if (HttpStatus.OK != response.getStatusCode()) {
                LOGGER.error(response.getBody().toString());
                throw new RuntimeException("Error getting employee data");
            }

            return Optional.of(EmployeeMapper.fromEntityApi(response.getBody().getEmployee()));

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new RuntimeException("Error getting employee data");
        }
    }

    @Override
    public List<Employee> findAll() {
        try {
            ResponseEntity<ListEmployeeResponse> response = restTemplate
                    .getForEntity(EMPLOYEES_URL, ListEmployeeResponse.class);

            if (HttpStatus.OK != response.getStatusCode()) {
                LOGGER.error(response.getBody().toString());
                throw new RuntimeException("Error getting employees data");
            }

            return response.getBody()
                    .getEmployees().stream().map(EmployeeMapper::fromEntityApi)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new RuntimeException("Error getting employees data");
        }

    }
}
