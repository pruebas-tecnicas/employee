package com.co.lbc.app.aplication.domain;

import com.co.lbc.app.application.shared.domain.DomainError;

public class EmployeeNotFound extends DomainError {

    public EmployeeNotFound(String message) {
        super(message);
    }
}
