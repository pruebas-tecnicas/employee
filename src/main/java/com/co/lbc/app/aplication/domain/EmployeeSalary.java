package com.co.lbc.app.aplication.domain;

import java.math.BigDecimal;

public class EmployeeSalary {

    private BigDecimal value;

    public EmployeeSalary(BigDecimal value) {
        this.value = value;
    }

    BigDecimal multiply(BigDecimal multiplier) {
        return this.value.multiply(multiplier);
    }

    public BigDecimal value() {
        return value;
    }
}
