package com.co.lbc.app.aplication.domain;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository {

    Optional<Employee> findById(EmployeeId id);

    List<Employee> findAll();
}
