package com.co.lbc.app.aplication.domain;

import java.math.BigDecimal;

public class Employee {

    private EmployeeId id;

    private EmployeeName name;

    private EmployeeSalary salary;

    private EmployeeAge age;

    private EmployeeImage image;

    public Employee(EmployeeId id, EmployeeName name, EmployeeSalary salary, EmployeeAge age, EmployeeImage image) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.age = age;
        this.image = image;
    }

    public EmployeeId getId() {
        return id;
    }

    public EmployeeName getName() {
        return name;
    }

    public EmployeeSalary getSalary() {
        return salary;
    }

    public EmployeeAge getAge() {
        return age;
    }

    public EmployeeImage getImage() {
        return image;
    }

    public BigDecimal anualSalary() {
        return this.salary.multiply(new BigDecimal(12));
    }


}
