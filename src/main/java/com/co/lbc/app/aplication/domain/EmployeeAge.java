package com.co.lbc.app.aplication.domain;

public class EmployeeAge {

    private final int value;

    public EmployeeAge(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }
}
