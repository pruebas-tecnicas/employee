package com.co.lbc.app.aplication.domain;

public class EmployeeName {

    private final String value;

    public EmployeeName(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }
}
