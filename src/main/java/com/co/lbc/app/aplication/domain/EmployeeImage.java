package com.co.lbc.app.aplication.domain;

public class EmployeeImage {
    private final String value;

    public EmployeeImage(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }
}
