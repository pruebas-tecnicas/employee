package com.co.lbc.app.aplication.domain;

import java.util.Objects;

public class EmployeeId {

    private Long value;

    public EmployeeId(Long value) {
        this.value = value;
    }

    public Long value() {
        return this.value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeId that = (EmployeeId) o;
        return value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
