import React, { useState } from "react";
import { InputGroup, Button, Input, Form } from "reactstrap";
const EmployeeSearcher = ({handleSearch}) => {
  const [value, setValue] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    handleSearch(value);
  };
  const handleChange = (event) => {
    const { value } = event.target;
    setValue(value);
  };

  return (
    <>
      <Form onSubmit={handleSubmit}>
        <InputGroup>
          <Input
            placeholder="Id"
            type="number"
            onChange={handleChange}
            value={value}
          />
          <Button color="success" type="submit">
            Search
          </Button>
        </InputGroup>
      </Form>
    </>
  );
};

export default EmployeeSearcher;
