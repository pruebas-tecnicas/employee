import React from "react";
import { Container, Table, Row, Col } from "reactstrap";
import EmployeeSearcher from "./EmployeeSearcher";
import useEmployeesFetcher from "./useEmployeesFetcher";
import { NumericFormat } from 'react-number-format';

const EmployeesList = () => {
  const { employees, loading, setEmployeeId } = useEmployeesFetcher();

  const employeeList = employees.map((employee) => {
    return (
      <tr key={employee.id}>
        <td>{employee.id}</td>
        <td style={{ whiteSpace: "nowrap" }}>{employee.name}</td>
        <td><NumericFormat value={employee.salary} displayType={'text'} thousandSeparator={true} prefix={'$'} /></td>
        <td><NumericFormat value={employee.anual_salary} displayType={'text'} thousandSeparator={true} prefix={'$'} /></td>
        <td>{employee.age}</td>
      </tr>
    );
  });

  return (
    <div>
      <Container fluid="md" className="mt-4">
        <div className="float-end"></div>

        <Row>
            <Col xs = {12}  sm={8} md={9} lg={10}>
              <h3>Employees</h3>
            </Col>
            <Col xs = {12} sm={4} md={3} lg={2}>
              <EmployeeSearcher handleSearch={setEmployeeId} />
            </Col>
        </Row>


        {loading ? (
          <p>Loading...</p>
        ) : (
          <Table className="mt-4" responsive>
            <thead>
              <tr>
                <th width="10%">Id</th>
                <th width="40%">Name</th>
                <th width="20%">Salary</th>
                <th width="20%">Annual Salary</th>
                <th width="20%">Age</th>
              </tr>
            </thead>
            <tbody>
              {employeeList.length > 0 ? employeeList : <tr key={new Date().getTime()}><td colSpan="5">Not results found</td></tr>}
            </tbody>
          </Table>
        )}
      </Container>
    </div>
  );
};

export default EmployeesList;
