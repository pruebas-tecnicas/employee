import { useCallback, useEffect, useState } from "react";

const EMPLOYEES_URL = "http://localhost:8080/employees";

const useEmployeesFetcher = () => {
    const [employees, setEmployees] = useState([]);
    const [loading, setLoading] = useState(false);
    const [employeeId, setEmployeeId] = useState(null);

    const getEmployees = useCallback(async(employeeId) => {
        setLoading(true);

        const url = employeeId ? `${EMPLOYEES_URL}/${employeeId}` : EMPLOYEES_URL;

        try{
            const response = await fetch(url);
            if(!response.ok){
                throw new Error(response.message ? response.message : "Unknown error");
            }

            const data = await response.json();

            if(Array.isArray(data)){
                setEmployees(data);
            }else{
                setEmployees([data]);
            }
        }catch(e){
            setEmployees([])
        }finally{
            setLoading(false)
        }

    }, []);

    useEffect(() => {
        getEmployees(employeeId);
    }, [employeeId,getEmployees]);

    return {
        loading,
        employees,
        setEmployeeId
    };
};

export default useEmployeesFetcher;