import React from "react";
import ReactDOM from "react-dom/client";
import AppNavBar from "./AppNavbar"
import EmployeesList from "./sections/employees/EmployeesList";

function App() {
  return <>
     <AppNavBar />
     <EmployeesList/>
  </>;
}

const root = ReactDOM.createRoot(document.getElementById("react"));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);