# Employees

## Technologies:

- Java 11
- Spring Boot 2.7.12
- H2
- Maven
- React.js 18
- Bootstrap 5
- Junit 5
- RestTemplate

## Execution:

The app is packaged as a war, so in order to run it, you only need to execute the command below:

```sh
./mvnw clean package && java -jar target/app-0.0.1-SNAPSHOT.war
```

After running the app, open it on your browser using the url:

```sh
http://localhost:8080/

